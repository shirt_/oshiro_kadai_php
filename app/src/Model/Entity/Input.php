<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Input extends Entity {
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
