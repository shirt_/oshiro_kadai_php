<?php
namespace App\Model\Table;

use Cake\ORM\Query; 
use Cake\ORM\RulesChecker;
use Cake\ORM\Table; 
use Cake\Validation\Validator;
        
class ReplysTable extends Table {
    public function initialize(array $config) {
        $this->belongsTo('Inputs')
             ->setForeignKey('input_id')
             ->setJoinType('INNER');
        $this->belongsTo('Users')
             ->setForeignKey('user_id')
             ->setJoinType('INNER');
    } 
}
