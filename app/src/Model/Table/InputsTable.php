<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class InputsTable extends Table {
    public function initialize(array $config) {

        // Upload Plugin
       // $this->addBehavior('Josegonzalez/Upload.Upload', ['image' => [],]);

        $this->hasMany('Users')
             ->setForeignKey('input_id')
             ->setDependent(true);

        $this->hasMany('Replys')
             ->setForeignKey('input_id')
             ->setDependent(true);
    }
}
