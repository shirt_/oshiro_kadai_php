<?php
namespace App\Controller;

use App\Controller\AppController;

class InputsController extends AppController {

    public $paginate = ['limit' => 5, 'order' => ['inpts.input_id' => 'asc']];
    public $helpers = ['Html'];
    public function initialize() {
        $name = 'Inputs';
        $this->loadComponent('Paginator');
        $this->viewBuilder()->autoLayout = true;
        $this->viewBuilder()->layout('default');
    }

    public function index() {
       $query = $this->Inputs->find()->contain(['Users', 'Replys'])->all();
       $this->set('inputs', $query);
       $this->set('entity', $this->Inputs->newEntity());   
       $this->set('count', $query->count());
       $this->set('inputs', $this->paginate());
       if ($this->request->is('post')) {
           $data = $this->request->data();
           $b = mb_convert_kana($data['title'], 's');
           $b = trim(preg_replace('/\s+/', ' ', $b));
           $words = explode(' ', $b);
           foreach ($words as $key => $word) {
               $word_z = mb_convert_kana($word, 'A');
               $options['conditions']['Hoge.hoge like'] = "%{$word_z}%";
           }
           $input = $this->Inputs->find()->where(['title  LIKE' => '%'.$options['conditions']['Hoge.hoge like'].'%']);
           dump($input);
           exit;
       }
    }

    public function listinput($input_id) {
        $listdata = $this->Inputs->find()->where(['Inputs.input_id' => $input_id])->contain(['Users', 'Replys'])->first();
        $this->set('listinput', $listdata);
        $this->set('entity', $this->Inputs->newEntity());
    }

    public function addinput() {
         if ($this->request->is('post')) {
            $input = $this->Inputs->newEntity();
            $this->request->data['user_id'] = 1;
            $data = $this->request->getData();
            $input = $this->Inputs->patchEntity($input, $data);
            $input->photo = $data['photo']['name'];
            $dir = realpath(WWW_ROOT . "img");
            @move_uploaded_file($data['photo']['tmp_name'], $dir.'/'.$data['photo']['name']);
            //dump($data['photo']['tmp_name'], $dir.'/'.$data['photo']['name']);
            //exit;
            if ($this->Inputs->save($input)) {
                return $this->redirect(['action' => 'index']);
            }
        }
    }
}
