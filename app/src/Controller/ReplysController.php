<?php
namespace App\Controller;

class ReplysController extends AppController {
    public $helpers = ['Html'];

    public function initialize() {
        $this->name = 'Replys';
        $this->viewBuilder()->autoLayout(true);
        $this->viewBuilder()->layout('default');
    }

    public function replyinput($input_id) {
        $replyquery = $this->Replys->find()->where(['Replys.input_id' => $input_id])->contain(['Users', 'Inputs'])->all();
        $this->set('replys', $replyquery);
        $this->set('entity', $this->Replys->newEntity());

        if ($this->request->is('post')) {
            $return = $this->Replys->newEntity();
            $this->request->data['user_id'] = 2;
            $this->request->data['input_id'] = $input_id;
            $return = $this->Replys->patchEntity($return, $this->request->data);
            if ($this->Replys->save($return)) {
                return $this->redirect(['controller' => 'Inputs', 'action' => 'index']);
            }
        }
    }
}
