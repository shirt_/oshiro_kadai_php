<?php
namespace App\Controller;
use App\Form\ContactForm;

class UsersController extends AppController {
    public function initialize() {
    //Controller名
    $name = 'Users';
    //autolayout使用
    $this->viewBuilder()->autoLayout = true;
    //layoutを使用
    $this->viewBuilder()->layout('default');
    }

    public function index() {
        //日本語の曜日配列
        $weekjp = array(
            '日', //0
            '月', //1
            '火', //2
            '水', //3
            '木', //4
            '金', //5
            '土'  //6
        );
        //現在の曜日番号（日:0  月:1  火:2  水:3  木:4  金:5  土:6）を取得
        $weekno = date('w');
        //日本語曜日を出力
        $day = $weekjp[$weekno];
        //出力した日本語曜日をセットする
        $this->set('day', $day);
        $userdata = $this->Users->find('all');
        $this->set('userdata',$userdata);
        $this->set('count', $userdata->count());
    }
    
    public function login(){
    
    }
    
    public function regist() {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $session = $this->request->session();
            $session->write('data', $data);

            return $this->redirect(['action' => 'registconfirm']);
        }
    }

    public function registconfirm() {
       $session = $this->request->session();
       $data = $session->read('data');
       $this->set('inputs', $data);

       $session->write('data', $data);

       return $this->redirect(['action' => 'registcomp']);
    }

    public function registcomp() {
       $session = $this->request->session();
       $data = $session->read('data');
       if ($this->Users->save($data)) {
           return $this->redirect(['action' => 'login']);
       }
    }
    
    public function resetpass() {

    }

    public function resetpasscomp() {

    }
}
