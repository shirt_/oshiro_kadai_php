<div class='header'>
    <h2 class='logo'>ようこそ犬の掲示板へ！！</h2>
    <?php foreach ($userdata as $obj): ?>
        <?php $mailaddress = $obj->mailaddress ?>       
        <?php if ($mailaddress === "abctest@co.jp"): ?>
            <p class='top_name'>ようこそ<?= $obj->nickname ?>さん、犬の掲示板へ</p>
        <?php endif; ?>
    <?php endforeach; ?>
</div>

<div class="login_link">
    <a href="<?= $this->Url->build(['controller'=>'Users', 'action'=>'login']); ?>" style="margin-right: 20px;">ログイン</a>
    <a href="#" style="margin-left: 20px;">ヘルプ</a>
</div>
