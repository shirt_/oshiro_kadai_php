<?= $this->Html->css('addinput.css'); ?>
<?= $this->fetch('css') ?>

<div class="postinput_title">   
    <h2>自慢のペットの掲示板</h2>
</div>
<div class="input_title">
    <p>新規投稿</p>
</div>
<div class="input_kaisetu">
    <p>このFORMは開いてから30分すると無効になります。文章入力に時間がかかると予想されるときは、事前に用意した<br />
       別のファイルから「コピー＆ペースト」して下さい。<br />
       また、添付画像があるときは、その容量が制限（100KB以下）を超えないことを事前にご確認下さい。
    </p>
</div>
<div class="input_area">
    <?=  $this->Form->create($input, ['enctype' => 'multipart/form-data']); ?>
    <div>
        <dt class="input_daimei">題名</dt>
        <?= $this->Form->text('input_title', ['class' => 'input_daimei_title']); ?>
        <label class="daimei_help">（全角30字以下）</label>
    </div>
    <div>
        <dt class="area_comment">本文</dt><br> 
        <?= $this->Form->textarea('input_comment', ['class' => 'input_comment']); ?>
        <label class="comment_setumei">（文字数制限全角1000文字）</label><br>
        <label class="comment_setumei2">[制限を超えた文は尻切れになります。]</label>
    </div>
    <div>
        <label class="gazo_title">添付画像</label><br>
        <label class="gazo_setumei1">[あれば]</label>
        <p class="gazo_kaisetu">
            HTMLタグを埋めても機能しません。半角＄があると全角＄に置換されます。
        </p>
        <?= $this->Form->file('photo', ['class' => 'file_data']); ?>
        <p class="gazo_file_setumei">JPEG/ PNG/ GIF画像で容量が100KB以下のもの１ファイルに限ります。</p>
    </div>
    <?= $this->Form->submit('送信', ['class' => 'add_input']); ?>
    <?= $this->Form->end(); ?>
    <?= $this->Form->button('キャンセル',['class' => 'cancel', 'onclick' => 'history.back()']); ?>
</div>    
