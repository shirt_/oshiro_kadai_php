
    <div class="input_keijiban">
        <strong>自慢の掲示板</strong>
    </div>
    <div class="ichiran_btn">
        <a href="<?php echo $this->Url->build(['controller'=>'Inputs', 'action'=>'addinput']); ?>">✏️投稿する</a><br>    
    </div>
<table>
<tbody>
    <p>この掲示板の投稿者数：<?=$count ?>件</p>
    <?= $this->Form->create() ?>
        <?= $this->Form->input("title"); ?>
        <?= $this->Form->button('検索する') ?>
        <?= $this->Form->end() ?>
    <?php foreach ($inputs as $input): ?>
        <tr>
            <td><a href="<?= $this->Url->build(['controller'=>'Inputs', 'action'=>'listinput', $input->input_id]); ?>"><?= $input->input_title ?></a></td>
            <td>返信：（<?= count($input->replys) ?>）</td>
            <td><?= $input->input_date ?></td>
        </tr>
    <?php endforeach; ?>
</tbody>
<?= $this->Paginator->prev('<prev'); ?>
    <?= $this->Paginator->numbers(); ?>
    <?= $this->Paginator->next('next>'); ?>
</table>
