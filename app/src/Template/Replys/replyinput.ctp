
    <div class="return_title">
        <p>返信投稿</p>
    </div>
    <div class="retrun_area">
        <?php
            echo $this->Form->create();
            echo $this->Form->text('return_title', ['class' => 'return_title_mei']);
            echo $this->Form->textarea('return_comment', ['class' => 'return_comment']);
            echo $this->Form->submit('送信', ['class' => 'retrun_input']);

            echo $this->Form->end();
        ?>
    </div>
    <?php
        echo $this->Form->button('キャンセル',['class' => 'cancel', 'onclick' => 'history.back()']);
    ?>
    <div class="input_data">
        <p>以下は親記事のメッセージ内容です。</p>
    </div>
    <?php foreach ($replys as $reply): ?>
    <p>題名：<?= $reply->input->input_title ?> <?= $reply->user->nickname ?>さん <?= $reply->input->input_date ?></p>
    <p><?= $reply->input->input_comment ?></p>
    <?= $this->Html->image('images.jpeg', ['alt' => 'CakePHP']); ?>
    <p><?= $reply->return_comment ?></p>
    <?php endforeach; ?>
    
