<!TYPE html>
    <div class="post_title">
        <?php
            echo "自慢のペットの掲示板";
        ?>
    </div>
    <div class="input_title">
        <?php
            echo "新規投稿";
        ?>
    </div>
    <div class="input_kaisetu">
        <?php
            echo "このFORMは開いてから30分すると無効になります。文章入力に時間がかかると予想されるときは、事前に用意した<br />
                  別のファイルから「コピー＆ペースト」して下さい。<br />
                  また、添付画像があるときは、その容量が制限（100KB以下）を超えないことを事前にご確認下さい。";
        ?>
    </div>
    <form action="#" method="post">
        <p class="daimei">
            <label>題名<input type="dai" name"title" placeholder="" class="title_mei">
                <label class="input_title_moji">（全角30字以下）</label>
            </label>
        </p>
        <div class="comment">
            <label class="comment_ran">本文</label><br>
            <label class="comment_setumei">（文字数制限全角1000文字）</label><br>
            <label class="comment_setumei2">[制限を超えた文は尻切れになります。]</label>
            <textarea name="comment" row="10" cols="100" class="text_comment">
            </textarea><br>
        </div>
        <div class="gazo">
            <label class="gazo_title">添付画像</label><br>
            <label class="gazo_setumei1">[あれば]</label>
            <p class="gazo_kaisetu">
                <?php
                    echo "HTMLタグを埋めても機能しません。半角＄があると全角＄に置換されます。"
                ?>
            </p>
            <form class="gazo_file">
                <input type="file" name="ファイル呼び出し" size="30" class="gazo_file">
            </form>
            <p class="gazo_file_setumei">JPEG/ PNG/ GIF画像で容量が100KB以下のもの１ファイルに限ります。</p> 
        </div>
        <div class="botton">
            <input type="submit" class="cancel" name="cancel" value="キャンセル">
            <input type="submit" class="input" name="input" value="送信">
        </div>
    </form>            
