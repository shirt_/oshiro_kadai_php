<div class="link">
    <table>
        <tr>
            <ul id="link_hyo">
                <a href="#" id="a_link1">当サイトについて</a><br>
                <a href="#" id="a_link2">初めての方へ</a><br>
                <a href="#" id="a_link3">ルール</a><br>
                <a href="#" id="a_link4">よくある質問</a><br>
            </ul>
        </tr>
    </table>        
    <div class="count">
        <table>
            <tr>
                <ul id="count_hyo">
                    <p id="count_jikou1">
                        <?=date('Y/m/d ',time()).'('.$day.')' ?>
                    </p>
                    <p id="count_jikou2">あなたは</p>
                    <p id="count_jikou3"><?=$count ?>人目の</p>
                    <p id="count_jikou4">犬好きさんです。</p>
                </ul>
            </tr>
        </table>
    </div>
</div>
<div class="sait_kaisetu">
    <p>ここでは、ワンちゃん大好きの人が愛犬の日常や仕草や自慢などをお話しするサイトになります。<br>
       他のお友達とお話ししたり些細な事でも投稿したい方は<font color="red">掲示板へ</font>をクリ
       ックしてください。<br>
       皆さんで楽しく情報を共有しましょう。<br>
    </p>
</div>
<br><br>
<div class="keijiban_btn">
    <a href="<?php echo $this->Url->build(['controller'=>'Inputs', 'action'=>'index']); ?>">掲示板へ</a><br>
</div>
