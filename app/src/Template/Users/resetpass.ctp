<?= $this->Html->css('resetpass.css'); ?>
<?= $this->fetch('css') ?>

<h2 class="passtitle">パスワード再設定</h2>
<p class="pass_setumei">パスワードを再設定します。<br>
   ニックネーム、メールアドレスは忘れずにご入力下さい。</p>

<?= $this->Form->create() ?>
<div class="passname">
    <?= $this->Form->input('ニックネーム', ['class' => 'nickname']) ?>
</div>
<div class="passmail">
    <?= $this->Form->input('メールアドレス', ['class' => 'pass_mail']) ?>
</div>
<div class="newpassword">
    <?= $this->Form->input("password",["label" => '新しいパスワード', 'class' => 'new_pass']) ?>
</div>
<div class="kakuninpassword">
    <?= $this->Form->input("checkpassword", ["label" => '新しいパスワード確認', 'class' => 'kakunin_pass']) ?>
</div>
<?= $this->Form->button('再設定する',['class' => 'resetbutton']) ?>
<?= $this->Form->end() ?>
