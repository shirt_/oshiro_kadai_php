<h2>パスワード再設定完了</h2>

<p>パスワードの設定が完了いたしました。</p>

<div class="comp_btn">
    <a class="complet" href="<?= $this->Url->build(['controller'=>'Users', 'action'=>'login']); ?>">今すぐログインする</a><br>
</div>
