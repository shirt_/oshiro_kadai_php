<?= $this->Html->css('regist.css'); ?>
<?= $this->fetch('css') ?>

<h2 class="regist_title">会員登録情報</h2>
<p class="regist_msg">以下のフォームに必要事項を全て入力ください。<br>
   ログイン時のIDはメールアドレスをご利用ください。</p>

<?= $this->Form->create(); ?>
<div class="regist_name">
    <?= $this->Form->text("nickname", ["label" => 'ニックネーム', 'class' => 'registname']) ?>
</div>
<div class="regist_pass">
    <?= $this->Form->text("password", ["label" => 'パスワード', "type" => "password", 'class' => 'registpass']) ?>
</div>
<div class="regist_mail">
    <?= $this->Form->text("mailaddress", ["label" => 'メールアドレス', 'class' => 'registmail']) ?>
</div>
<?= $this->Form->button('この内容で確認する',['class' => 'resetbutton']) ?>
<?= $this->Form->button('全てクリア',["type" => "reset", 'class' => 'clerabutton']) ?>
<?= $this->Form->end() ?>

