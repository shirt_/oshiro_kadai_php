<?= $this->Html->css('login.css'); ?>
<?= $this->fetch('css') ?>

<?= $this->Form->create() ?>
<div class="mail_input">
    <?= $this->Form->input('メールアドレス', ['class' => 'mail']) ?>
</div>
<div class="password_input">
    <?= $this->Form->input('パスワード',['class' => 'password']) ?>
</div>
<div class="login_button">
    <?= $this->Form->button('ログイン',['class' => 'login']) ?>
</div>
    <?= $this->Form->end() ?>
<div class="back_button">
    <?= $this->Form->button('戻る',['class' => 'logback', 'onclick' => 'history.back()']); ?>
</div>
<h3 class="nosinki_user">新規登録を行っていない方</h3>
<div class="shinki_btn">
    <a class="shinki_toroku" href="<?= $this->Url->build(['controller'=>'Users', 'action'=>'regist']); ?>">会員登録</a><br>
</div>

<h3 class="nopassword_user">パスワードを忘れた方</h3>
<div class="password_btn">
    <a href="<?= $this->Url->build(['controller'=>'Users', 'action'=>'resetpass']); ?>">パスワードを忘れた場合</a><br>
</div>
